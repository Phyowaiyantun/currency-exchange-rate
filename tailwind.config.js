/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./src/**/*.{js,jsx,ts,tsx}"],
  darkMode: "class",
  daisyui: {
    themes: [
      // "light",
      // "dark",
      // "cupcake",
      {
        cex: {
          primary: "#1d4ed8",

          secondary: "#f59e0b",

          accent: "#1fb2a6",

          neutral: "#2a323c",

          "base-100": "#ffffff",

          info: "#3abff8",

          success: "#10b400",

          warning: "#facc15",

          error: "#dc2626",
        },
      },
    ],
  },
  theme: {
    container: {
      center: true,
      padding: {
        DEFAULT: "1rem",
        sm: "2rem",
        lg: "4rem",
        xl: "10rem",
        // "2xl": "10rem",
      },
    },

    extend: {
      fontFamily: {
        sans: ["Poppins", "sans-serif"],
      },
    },
  },
  plugins: [require("daisyui"), require("autoprefixer")],
};
