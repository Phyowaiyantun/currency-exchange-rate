import React, { useState, useEffect } from "react";

function Toaster({ type, message, className = "" }) {
  const [isVisible, setIsVisible] = useState(true);

  useEffect(() => {
    const timeoutId = setTimeout(() => {
      setIsVisible(false);
    }, 8000);

    return () => clearTimeout(timeoutId);
  }, []);

  return (
    <>
      {isVisible && (
        <div className="toast toast-top toast-end">
          <div
            className={`alert ${!isVisible ? "fade-out" : ""} ${
              type === "success" ? "alert-success" : "alert-error"
            }  ${className}`}
          >
            <span>{message}</span>
          </div>
        </div>
      )}
    </>
  );
}

export default Toaster;
