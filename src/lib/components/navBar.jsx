import logo from "../../assets/images/logo.png";
import { FaGitlab } from "react-icons/fa6";

function NavBar() {
  return (
    <div className="absolute top-0 navbar">
      <div className="container">
        <div className="flex !justify-between w-full">
          <div className="w-[10rem]">
            <img
              src={logo}
              alt="CurrencyEx"
              className="w-full font-semibold text-center"
            />
          </div>

          <a
            href="https://gitlab.com/Phyowaiyantun/currency-exchange-rate"
            rel="noreferrer"
            target="_blank"
            className="normal-case btn btn-ghost hover:text-white"
          >
            <FaGitlab />
            Repository
          </a>
        </div>
      </div>
    </div>
  );
}

export default NavBar;
