/* eslint-disable array-callback-return */
import React, { memo, useEffect, useState } from "react";
import { useSelector } from "react-redux";
import CurrencyFlag from "react-currency-flags";

import { API_HOST } from "../../settings";
import fetchApiData from "../functions/fetchData";

function CurrencyGrid() {
  const {
    savedCurrencyCode,
    inputAmount,
    currencyCodeList,
    currencyCodeObj,
    currencyCardGrid,
  } = useSelector((state) => state.currencyListController);

  const [recallOnFailed, setRecallOnFailed] = useState(false);
  const [currencyData, setCurrencyData] = useState({});

  const handleFetchCurrencyData = () => {
    const params = {
      source: savedCurrencyCode,
    };

    const fetchData = fetchApiData(`${API_HOST}/change`, params);
    fetchData.then((e) => {
      if (e.success === true) {
        setCurrencyData(e.quotes);
      } else {
        setRecallOnFailed(true);
      }
    });
  };

  useEffect(() => {
    if (recallOnFailed) {
      setRecallOnFailed(false);
      handleFetchCurrencyData();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [recallOnFailed]);

  useEffect(() => {
    if (savedCurrencyCode) {
      handleFetchCurrencyData();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [savedCurrencyCode]);

  return (
    <div
      className={`grid  gap-4 lg:grid-cols-4 ${
        currencyCardGrid
          ? "grid-cols-2 md:grid-cols-3 lg:grid-cols-5"
          : "grid-cols-1 md:grid-cols-2 lg:grid-cols-3"
      }`}
    >
      {currencyCodeList.map((code, index) => {
        const key = `${savedCurrencyCode.toString() + code.toString()}`;

        const cData = currencyData[key];
        let calculatedValue = 0;
        if (cData && !inputAmount) {
          calculatedValue =
            cData.end_rate < 99
              ? parseFloat(cData.end_rate.toFixed(4))
              : Math.ceil(cData.end_rate);
        }
        if (cData && inputAmount) {
          var multiplier = cData.end_rate * inputAmount;
          if (multiplier < 1) {
            calculatedValue = parseFloat(multiplier.toFixed(4));
          } else if (multiplier < 99) {
            calculatedValue = parseFloat(multiplier.toFixed(2));
          } else {
            calculatedValue = Math.ceil(multiplier);
          }
        }
        if (savedCurrencyCode !== code && code !== "SVC") {
          return (
            <div
              key={index}
              className="w-full h-auto px-4 py-3 border-2 rounded-xl card card-compact bg-slate-50"
            >
              {cData ? (
                <div
                  className={`flex ${
                    currencyCardGrid ? "flex-col pt-2" : ""
                  }  items-center w-full gap-4 `}
                >
                  <CurrencyFlag
                    currency={code}
                    size="xl"
                    className="bg-center rounded-md"
                    title={currencyCodeObj[code]}
                  />
                  <div className="flex items-start justify-between w-full">
                    <div className="flex flex-col gap-0">
                      <span className="text-lg font-semibold leading-5 whitespace-nowrap ">
                        {calculatedValue.toLocaleString()} {code}
                      </span>
                      <span className="text-[0.8rem] text-slate-500">
                        {code}/{savedCurrencyCode}
                      </span>
                    </div>
                    <span
                      className={`flex items-center ${
                        parseFloat(cData.change_pct.toFixed(2)) >= 0
                          ? "text-success"
                          : "text-error"
                      }`}
                    >
                      {cData.change_pct > 0 && "+"}
                      {parseFloat(cData.change_pct.toFixed(2))}%
                    </span>
                  </div>
                </div>
              ) : (
                <div className="flex items-start justify-between w-full">
                  <div
                    className={`flex ${
                      currencyCardGrid ? "flex-col pt-2" : ""
                    }  items-center w-full gap-4 animate-pulse `}
                  >
                    <div className="w-10 h-8 rounded-sm bg-base-300"></div>
                    <div className="flex items-start justify-between flex-grow w-full ">
                      <div className="flex flex-col gap-2">
                        <div className="h-3 w-[5rem] col-span-2 rounded-full bg-base-300"></div>
                        <div className="h-3 w-[5rem] col-span-1 rounded-full bg-base-300"></div>
                      </div>
                      <div className="w-5 h-2 rounded bg-base-300"></div>
                    </div>
                  </div>
                </div>
              )}
            </div>
          );
        }
      })}
    </div>
  );
}

export default memo(CurrencyGrid);
