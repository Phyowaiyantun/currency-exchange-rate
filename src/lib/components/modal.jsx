import React, { useEffect, useState, memo } from "react";
import ReactDOM from "react-dom";
import { useDispatch, useSelector } from "react-redux";
import CurrencyFlag from "react-currency-flags";
import { API_HOST } from "../../settings";
import {
  setCurrencyCodeList,
  setCurrencyCodeObj,
  setSavedCurrencyCode,
} from "../redux/controllers/currencyListController";
import saveCurrencyToLocal from "../functions/saveCurrencyToLocalStorage";
import Toaster from "./toast";
import fetchApiData from "../functions/fetchData";

function CurrencySelectModal(props) {
  const [searchTerm, setSearchTerm] = useState("");
  const [recallOnFailed, setRecallOnFailed] = useState(false);
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(false);
  const [cancelButtonAvailable, setCancelButtonAvailable] = useState(false);

  const dispatch = useDispatch();

  const CurrencyCodes = useSelector(
    (state) => state.currencyListController.currencyCodeList
  );

  const CurrencyPairs = useSelector(
    (state) => state.currencyListController.currencyCodeObj
  );

  const handleFetchCurrencyList = () => {
    const fetchData = fetchApiData(`${API_HOST}/list`);
    fetchData
      .then((e) => {
        if (e.success === true) {
          const resource = e.currencies || {};
          setLoading(false);
          dispatch(setCurrencyCodeObj(resource));
          dispatch(setCurrencyCodeList(Object.keys(resource) || []));
        } else {
          setRecallOnFailed(true);
        }
      })
      .catch((error) => {
        setLoading(false);
        setError(error);
      });
  };

  useEffect(() => {
    if (recallOnFailed) {
      setRecallOnFailed(false);
      handleFetchCurrencyList();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [recallOnFailed]);

  useEffect(() => {
    setLoading(true);
    handleFetchCurrencyList();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    const savedCurrencyCode = localStorage.getItem("currencyCode");
    if (!savedCurrencyCode) {
      document.getElementById("currency_select_modal").showModal();
      const handleKeyDown = (event) => {
        if (event.key === "Escape") {
          event.preventDefault();
        }
      };
      window.addEventListener("keydown", handleKeyDown);
      return () => {
        window.removeEventListener("keydown", handleKeyDown);
      };
    } else {
      setCancelButtonAvailable(true);
      dispatch(setSavedCurrencyCode(savedCurrencyCode));
    }
  }, [dispatch]);

  const filteredCurrencyCodes = CurrencyCodes.filter((code) => {
    const currencyName = CurrencyPairs[code].toLowerCase();
    const currencyCode = code.toLowerCase();

    return (
      currencyName.includes(searchTerm.toLowerCase()) ||
      currencyCode.includes(searchTerm.toLowerCase())
    );
  });

  const handleCurrencySaving = (code) => {
    saveCurrencyToLocal(code);
    dispatch(setSavedCurrencyCode(code));
    document.getElementById("currency_select_modal").close();
    setSearchTerm("");
    props.reloadParent();
  };

  const toastPortal = error && <Toaster type="error" message={error.message} />;

  return (
    <>
      <dialog
        id="currency_select_modal"
        className="modal modal-bottom sm:modal-middle"
      >
        <div className="overflow-hidden modal-box">
          <h3 className="text-lg font-bold text-center">
            Select your currency
          </h3>
          {cancelButtonAvailable && (
            <button
              onClick={() =>
                document.getElementById("currency_select_modal").close()
              }
              className="absolute btn btn-sm btn-circle right-4 top-4"
            >
              ✕
            </button>
          )}
          <div className="modal-action">
            <div className="flex flex-col w-full gap-4">
              <input
                type="text"
                placeholder="Search currency"
                value={searchTerm}
                onChange={(e) => setSearchTerm(e.target.value)}
                className="w-full input input-bordered input-primary"
              />
              <div className="flex flex-col h-[20rem] overflow-y-scroll overflow-x-hidden p-1">
                {!loading ? (
                  <>
                    {filteredCurrencyCodes.length > 0 ? (
                      <div className="grid grid-cols-3 gap-2 md:grid-cols-4">
                        {filteredCurrencyCodes.map((code, index) => (
                          <div key={index}>
                            {code !== "SVC" && (
                              <span
                                onClick={() => handleCurrencySaving(code)}
                                title={CurrencyPairs[code]}
                                className="flex justify-start h-auto min-h-0 px-4 py-3 rounded-md bg-slate-200 hover:ring-2 ring-offset-2 ring-secondary btn"
                              >
                                <CurrencyFlag currency={code} size="sm" />
                                {code}
                              </span>
                            )}
                          </div>
                        ))}
                      </div>
                    ) : (
                      <center className="font-semibold">
                        Result not found
                      </center>
                    )}
                  </>
                ) : (
                  <center className="font-semibold">Loading...</center>
                )}
              </div>
            </div>
          </div>
        </div>
      </dialog>
      {ReactDOM.createPortal(toastPortal, document.body)}
    </>
  );
}

export default memo(CurrencySelectModal);
