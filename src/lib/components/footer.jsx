function Footer() {
  return (
    <footer className="p-4 footer footer-center bg-primary text-white mt-[5rem]">
      <aside>
        <p>Copyright © 2023 - All right reserved by Phyo Wai Yan Tun</p>
      </aside>
    </footer>
  );
}

export default Footer;
