function saveCurrencyToLocal(currencyCode) {
  try {
    localStorage.setItem("currencyCode", currencyCode);
  } catch (error) {
    alert("Error saving currency code to local storage:", error);
  }
}

export default saveCurrencyToLocal;
