import axios from "axios";
import { API_ACCESS_KEY } from "../../settings";

async function fetchApiData(url, params) {
  const requiredParams = { ...params, access_key: API_ACCESS_KEY };
  try {
    const { data } = await axios.get(url, { params: requiredParams });
    return data;
  } catch (e) {
    console.error(e);
  }
}

export default fetchApiData;
