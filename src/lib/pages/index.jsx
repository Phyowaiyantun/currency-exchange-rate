import { Helmet } from "react-helmet";
import CurrencySelectModal from "../components/modal";
import { useEffect, useState } from "react";
import CurrencyFlag from "react-currency-flags";
import { HiChevronUpDown, HiMiniAdjustmentsHorizontal } from "react-icons/hi2";
import CurrencyGrid from "../components/currencyGrid";
import { useDispatch, useSelector } from "react-redux";
import {
  setCurrencyCardGrid,
  setInputAmount,
} from "../redux/controllers/currencyListController";
import animationOne from "../../assets/lottie/animi-1.json";
import Lottie from "lottie-react";

function HomePage() {
  const [CurrentCurrencyCode, setCurrentCurrencyCode] = useState("");
  const [InputData, setInputData] = useState("");
  const dispatch = useDispatch();

  const { currencyCardGrid } = useSelector(
    (state) => state.currencyListController
  );

  const handleReload = () => {
    const savedCurrencyCode = localStorage.getItem("currencyCode");
    if (savedCurrencyCode) {
      setCurrentCurrencyCode(savedCurrencyCode);
    }
  };

  const handleInputChange = (e) => {
    const inputValue = e.target.value;

    // Use a regular expression to allow only numeric characters
    const numericValue = inputValue.replace(/[^0-9]/g, "");
    setInputData(numericValue);
  };

  useEffect(() => {
    dispatch(setInputAmount(InputData));
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [InputData]);

  const handleGridSettingChange = () => {
    dispatch(setCurrencyCardGrid(!currencyCardGrid));
  };

  useEffect(() => {
    handleReload();
  }, []);
  return (
    <>
      <Helmet>
        <html lang="en" />
        <title>CurrencyEx with React</title>
        <meta
          name="description"
          content="Currency Exchange App build with React"
        />
      </Helmet>
      <div className="flex flex-col">
        <section
          className="pt-[7rem] pb-[2rem] md:py-[7rem] w-full bg-opacity-10"
          style={{
            backgroundImage: "url('/gradient-bg.jpg')",
          }}
        >
          <div className="container">
            <div className="grid grid-cols-1 gap-6 md:grid-cols-2 md:gap-4">
              <div className="flex flex-col justify-center text-center md:text-left">
                <span className="flex text-[0.85rem] md:text-[1rem ] mb-1 justify-center md:mb-4 md:justify-start text-slate-500">
                  Developed by
                  <a
                    className="ml-1 underline hover:text-primary"
                    href="https://phyowaiyantunps.pythonanywhere.com"
                    target="_blank"
                    rel="noreferrer"
                  >
                    Phyo Wai Yan Tun
                  </a>
                </span>
                <h1 className="mb-4 text-4xl font-bold">
                  Explore Global Currencies with
                  <br />
                  <span className=" text-primary">
                    Real-time Exchange Rates
                  </span>
                </h1>
                <span className="md:text-lg text-gray-700 text-[0.9rem] ">
                  Discover the latest exchange rates and make informed decisions
                  for your international transactions.
                </span>
                {/* Other content of your home page */}
              </div>
              <div className="flex items-center justify-center w-full pt-5">
                <Lottie
                  animationData={animationOne}
                  style={{ width: "350px", height: "350px" }}
                />
              </div>
            </div>
          </div>
        </section>
        <section className="sticky top-0 z-40">
          <div className="w-full shadow-md bg-slate-100">
            <div className="container flex items-center py-4 md:justify-between ">
              <div className="flex flex-col w-full gap-3 md:flex-row">
                <div className="grid order-2 w-full grid-cols-2 gap-4 md:w-fit md:order-1 md:grid-cols-1">
                  <button
                    type="button"
                    onClick={() =>
                      document
                        .getElementById("currency_select_modal")
                        .showModal()
                    }
                    className=" btn w-full flex items-center text-[1rem] px-0 md:p-2 md:w-[8rem]  bg-white shadow-md"
                  >
                    <CurrencyFlag currency={CurrentCurrencyCode} size="md" />

                    {CurrentCurrencyCode}
                    <HiChevronUpDown />
                  </button>
                  <button
                    type="button"
                    onClick={() => handleGridSettingChange()}
                    className={`text-slate-500  flex md:hidden normal-case btn w-full items-center text-[1rem] md:text-[0.9rem] px-0 md:p-2 md:w-[8rem]  bg-white ${
                      currencyCardGrid ? "shadow-inner" : "shadow-md"
                    }`}
                  >
                    View:{" "}
                    <HiMiniAdjustmentsHorizontal className="text-lg stroke-1 text-neutral" />
                  </button>
                </div>

                <input
                  type="text"
                  placeholder="Enter Amount"
                  className="input md:order-2  w-full shadow-md md:max-w-[20rem]"
                  value={InputData}
                  onChange={handleInputChange}
                />
              </div>
              <button
                type="button"
                onClick={() => handleGridSettingChange()}
                className={`text-slate-500  md:flex hidden normal-case btn w-full items-center text-[1rem] md:text-[0.9rem] px-0 md:p-2 md:w-[8rem]  bg-white ${
                  currencyCardGrid ? "shadow-inner" : "shadow-md"
                }`}
              >
                View:{" "}
                <HiMiniAdjustmentsHorizontal className="text-lg stroke-1 text-neutral" />
              </button>
            </div>
          </div>
        </section>
        <section className="pt-10">
          <div className="container">
            <CurrencyGrid />
          </div>
        </section>
      </div>
      <CurrencySelectModal reloadParent={handleReload} />
    </>
  );
}

export default HomePage;
