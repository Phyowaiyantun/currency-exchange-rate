import { createSlice } from "@reduxjs/toolkit";

export const currencyListControllerSlice = createSlice({
  name: "currencyListController",
  initialState: {
    savedCurrencyCode: "",
    currencyCodeList: [],
    currencyCodeObj: {},
    inputAmount: 1,
    currencyCardGrid: false,
  },
  reducers: {
    setSavedCurrencyCode: (state, action) => {
      state.savedCurrencyCode = action.payload;
    },
    setCurrencyCardGrid: (state, action) => {
      state.currencyCardGrid = action.payload;
    },
    setCurrencyCodeList: (state, action) => {
      state.currencyCodeList = action.payload;
    },
    setCurrencyCodeObj: (state, action) => {
      state.currencyCodeObj = action.payload;
    },
    setInputAmount: (state, action) => {
      state.inputAmount = action.payload;
    },
    clearCurrencyCodeList: (state) => {
      state.currencyCodeList = [];
    },
    clearCurrencyCodeObj: (state) => {
      state.currencyCodeObj = {};
    },
    clearSavedCurrencyCode: (state) => {
      state.token = "";
    },
    clearInputAmount: (state) => {
      state.inputAmount = 1;
    },
  },
});

export const {
  setSavedCurrencyCode,
  setCurrencyCodeList,
  setCurrencyCodeObj,
  setInputAmount,
  setCurrencyCardGrid,
  clearCurrencyCodeList,
  clearCurrencyCodeObj,
  clearSavedCurrencyCode,
  clearInputAmount,
} = currencyListControllerSlice.actions;

export default currencyListControllerSlice.reducer;
