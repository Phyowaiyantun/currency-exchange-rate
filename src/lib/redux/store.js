import { configureStore } from "@reduxjs/toolkit";
import currencyListControllerReducer from "./controllers/currencyListController";

export default configureStore({
  reducer: {
    currencyListController: currencyListControllerReducer,
  },
});
