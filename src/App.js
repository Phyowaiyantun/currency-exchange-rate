import { BrowserRouter, Route, Routes } from "react-router-dom";
import "./App.css";
import HomePage from "./lib/pages";
import MainLayout from "./lib/layout";
import PageNotfound from "./lib/pages/pageNotFound";

function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<MainLayout />}>
          <Route index element={<HomePage />} />
        </Route>
        <Route path="*" element={<PageNotfound />} />
      </Routes>
    </BrowserRouter>
  );
}

export default App;
